package viraglada;
public class Virag {
    private String fajta;
    private Integer porzoSzam;
    private String szin;
    private Boolean kinyilt;
    
    public Virag(){
        this.fajta = "Lágyszárú növény";
        this.porzoSzam = 0;
        this.szin = "zöld";
        this.kinyilt = Boolean.FALSE; // = false
    }
    
    public Virag(String fajta, Integer porszoSzam, String szin, Boolean kinyilt){
        this.fajta = fajta;
        this.kinyilt = kinyilt;
        this.porzoSzam = porszoSzam;
        this.szin = szin;
    }
    
    public Integer getPorzoSzam(){
        return this.porzoSzam;
    }

    public String getFajta() {
        return this.fajta;
    }

    public String getSzin() {
        return this.szin;
    }

    public Boolean isKinyilt() {
        return this.kinyilt;
    }
    
    public void setSzin(String szin){
        this.szin = szin;
    }
    
    public void kinyilik(){
        this.kinyilt = Boolean.TRUE;
    }
    
    public void pluszPorzo(Integer ujPorzokSzama){
        //max 20 porzója van a virágoknak!
        if(this.porzoSzam + ujPorzokSzama <= 20){
            this.porzoSzam += ujPorzokSzama;
        }
    }
    
    public void porzoLetepes(Integer letepettPorzokSzama){
        if(this.porzoSzam - letepettPorzokSzama >= 0){
            this.porzoSzam -= letepettPorzokSzama;
        }
    }
    
    
    
}
